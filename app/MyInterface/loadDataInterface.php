<?php

namespace App\MyInterface;

interface loadDataInterface{
	
	public function loadQuestions();
	public function loadAnswers($id);
}
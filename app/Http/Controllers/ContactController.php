<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Application;
use Tymon\JWTAuth\Facades\JWTAuth;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Http\JsonResponse;
use App\Http\Controllers\Controller;

use App\Mail\ContactUs;
use Illuminate\Support\Facades\Mail;

class ContactController extends Controller{

    public function sendEmail(Request $request){
        if(JWTAuth::authenticate($_GET['token'])){  
            $this->validate($request, [
                'phone_no' => 'required',
                'name' => 'required',
                'let_meet_you' => 'required',
                'message' => 'required',
		    ]);  

            $name = $request->name;
            $email = $request->email;
            $phone_no = $request->phone_no;
            $lets_meet_you = $request->let_meet_you;
            $msg = $request->message;

            $subject = "FindNaija Contact";

            Mail::send('contactMail', 
                [
                    'name' => $name,
                    'email' => $email,
                    'phone_no' => $phone_no,
                    'lets_meet_you' => $lets_meet_you,
                    'msg' =>$msg
                ], function($message){

                    $message->from('mail@findnaija.com');
                    $message->to('ogunbayo.abayomi@yahoo.com')->subject('Message From FINDNAIJA Contact Page');

                });

                return new JsonResponse(['message' => true]);
            
        }
    }
}


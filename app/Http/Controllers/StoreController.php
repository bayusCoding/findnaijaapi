<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Application;
use Tymon\JWTAuth\Facades\JWTAuth;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Http\JsonResponse;
use App\Http\Controllers\Controller;
use App\Models\Store;
use Validator;



class StoreController extends Controller
{
	private $store;

	public function __construct(Store $store)
    {
		$this->store = $store;
	}

    /**
     * Add Store location. 
     * @param $request
     */
    public function addStore(Request $request)
    {
        if(JWTAuth::parseToken()){
            $this->validate($request, [
		        'store_name' => 'required',
                'address' => 'required',
	            'category_id' => 'required',
                'area_id' => 'required',
                'description' => 'required',
                'latitude' => 'required',
                'longitude' => 'required'
		    ]);  

            $store_name = $request->store_name;
            $address = $request->address;
            $category_id = $request->category_id;
            $area_id = $request->area_id;
            $description = $request->description;
            $latitude = $request->latitude;
            $longitude = $request->longitude;

            if($this->store->addStore($store_name,$address,$category_id,$area_id,$description,$latitude,$longitude))
                return new JsonResponse(['message' => 'store added']);
        }
    }

    public function deleteStore($id)
    {
        if(JWTAuth::parseToken()){  
            if($this->store->deleteStore($id))
                return new JsonResponse(['message' => 'store deleted']);

            return new JsonResponse(['message' => false]);
        }
    }

    public function loadStore(Request $request){
         if(JWTAuth::authenticate($_GET['token'])){  
            // $this->validate($request, [
	        //     'category_id' => 'required',
            //     'area_id' => 'required',
		    // ]);  
            $rules = array(
                'category_id' => 'required',
                'area_id' => 'required',
		    );

            $validator = Validator::make($request->all(), $rules);

            if($validator->fails())
            {
                return new JsonResponse($validator->errors());
            }
            else
            {

                $category_id = $request->category_id;
                $area_id = $request->area_id;
                $store = $this->store->loadStore($category_id, $area_id);
                if($store)
                    return new JsonResponse($store);

                return new JsonResponse(['message'=>false]);

            }
        }
    }
}
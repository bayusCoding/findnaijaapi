<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Application;
use Tymon\JWTAuth\Facades\JWTAuth;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Http\JsonResponse;
use App\Http\Controllers\Controller;
use App\Models\Area;
 

class AreaController extends Controller{
    private $area;

	public function __construct(Area $area)
    {
		$this->area = $area;
	}

    /**
     * Add Area.
     * @param $request
     */
    public function addArea(Request $request)
    {
        if(JWTAuth::parseToken()){  
            $this->validate($request, [
	            'area_name' => 'required',
		    ]);  

             if($this->area->addArea($request->area_name))            
                return new JsonResponse(['message' => 'area added']);

            return new JsonResponse(['message'=>false]);
        }
    }

    public function deleteArea($id)
    {
        if(JWTAuth::parseToken()){ 
            if($this->area->deleteArea($id))
                return new JsonResponse(['message' => 'area deleted']);

            return new JsonResponse(['message' => false]);
        }
    }

    public function loadArea(Request $request)
    {
        if(JWTAuth::authenticate($_GET['token'])){  
            $area = $this->area->loadArea();
            if($area) 
                return new JsonResponse($area);

            return new JsonResponse(['message' => false]);
        }
    }
} 
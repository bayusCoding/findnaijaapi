<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Application;
use Tymon\JWTAuth\Facades\JWTAuth;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Http\JsonResponse;
use App\Http\Controllers\Controller;
use App\Models\Category;


class CategoryController extends Controller{
    private $category;

	public function __construct(Category $category)
    {
		$this->category = $category;
	}

    /**
     * Add Category location.
     * @param $request
     */
    public function addCategory(Request $request)
    {
        if(JWTAuth::parseToken()){  
            $this->validate($request, [
	            'category_name' => 'required',
		    ]);  

           if($this->category->addCategory($request->category_name))
                return new JsonResponse(['message' => 'category added']);

            return new JsonResponse(['message'=>false]);
        }
    }

    public function deleteCategory($id)
    {
        if(JWTAuth::parseToken()){  
            if($this->category->deleteCategory($id))
                return new JsonResponse(['message' => 'category deleted']);

            return new JsonResponse(['message'=>false]);
        }
    }
 
    public function loadCategory(Request $request)
    {
        if(JWTAuth::authenticate($_GET['token'])){ 
            $category = $this->category->loadCategory();
            if($category)
                return new JsonResponse($category);

            return new JsonResponse(['message'=>false]);

        }
    }

}
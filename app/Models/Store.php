<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Store extends Model
{
    protected $table = 'store';
 
    protected $fillable = ['store_name','address','category_id','area_id','description','lat','lng'];	

    public $timestamps = false;

    /**
     * Add Store location. 
     *
     */
    public function addStore($store_name,$address,$category_id,$area_id,$description,$latitude,$longitude){
        $store = DB::insert('INSERT INTO store(store_name,address,category_id,area_id,description,latitude,longitude)
                 VALUES(?,?,?,?,?,?,?)',[$store_name,$address,$category_id,$area_id,$description,$latitude,$longitude]);
		if($store)
			return $store;
		
		return false;
    }

    public function deleteStore($id){
        $del = DB::delete("DELETE FROM store WHERE store_id = ?", [$id]);
		if($del)
			return true;
		
		return false;
    }

    public function loadStore($category_id, $area_id){
        $store = DB::select(sprintf(
            "SELECT store_name,address,description,latitude,longitude FROM store 
                WHERE category_id=%u AND area_id=%u",$category_id, $area_id
            ));
		if($store)
			return $store;
		
		return false;
    }
} 

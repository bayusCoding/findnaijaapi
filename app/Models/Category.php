<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Category extends Model
{
    protected $table = 'category';
 
    protected $fillable = ['category_name'];	

    public $timestamps = false;


    public function addCategory($category_name){
        $category = DB::insert('INSERT INTO category(category_name) VALUES(?)',[$category_name]);
		if($category)
			return $category;
		
		return false;
    }

    public function deleteCategory($id){
        $del = DB::delete("DELETE FROM category WHERE category_id = ?", [$id]);
		if($del)
			return true;
		
		return false;
    }

    public function loadCategory(){
        $category = DB::select("SELECT * FROM category");
		if($category)
			return $category;
		
		return false;
    }
} 

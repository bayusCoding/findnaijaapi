<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Area extends Model
{
    protected $table = 'area';
 
    protected $fillable = ['area_name'];	

    public $timestamps = false;

    public function addArea($area_name){
        $area = DB::insert('INSERT INTO area(area_name) VALUES(?)',[$area_name]);
		if($area)
			return $area;
		
		return false;
    }

    public function deleteArea($id){
        $del = DB::delete("DELETE FROM area WHERE area_id = ?", [$id]);
		if($del)
			return true;
		
		return false;
    }

    public function loadArea(){
        $area = DB::select("SELECT * FROM area");
		if($area)
			return $area;
		
		return false;
    }
}

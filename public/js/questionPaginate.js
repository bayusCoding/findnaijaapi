$(document).ready(function(){

	$('div:eq(1)').show();
	$('.prev:first').hide();
	$('.next:last').hide();

	$('.next').click(function(){
		$(this).closest('div').hide();
	});
	$('.next').click(function(){
		$(this).closest('div').next().show();
	});
	
	$('.prev').click(function(){
		$(this).closest('div').hide();
	});
	
	$('.prev').click(function(){
		$(this).closest('div').prev().show();
	});

	$('#runner').runner({
	autostart: true,
    countdown: true,
	milliseconds: false,
    startAt: 40000,
    stopAt: 0
	}).add('#formvalue').trigger('submit');
});
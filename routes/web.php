<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['prefix' => 'api/', 'middleware' => 'cors'], function () {
	Route::post('/auth/login', [
        'as' => 'api.auth.login',
        'uses' => 'Auth\AuthController@postLogin',
    ]);

    Route::post('/auth/user', [
        'as' => 'api.auth.user',
        'uses' => 'Auth\AuthController@addUser',
    ]);

    Route::get('/auth/testing', [
        'as' => 'api.auth.testing',
        'uses' => 'Auth\AuthController@testing',
    ]);
////////////////////////////////////////////////////////////////////////////////
//                                                                            //  
//                                                                            //
//                     AUTHENTICATE WITH URL STRING                           //
//                                                                            //
//                                                                            //
////////////////////////////////////////////////////////////////////////////////

    Route::get('/area', [
        'as' => 'api.loadArea',
        'uses' => 'AreaController@loadArea',
    ]);
       
    Route::get('/category', [
            'uses' => 'CategoryController@loadCategory',
            'as' => 'api.loadCategory'
    ]);


    Route::get('/store', [
        'uses' => 'StoreController@loadStore',
        'as' => 'api.loadStore'
    ]);

    ////////E-mail
    Route::get('/email', [
            'uses' => 'ContactController@sendEmail',
            'as' => 'api.sendEmail'
    ]);
});



 Route::group(['prefix' => 'api/', 'middleware' => ['jwt.auth','cors']], function () {
    Route::post('/add_store', [
            'uses' => 'StoreController@addStore',
            'as' => 'api.addStore'
    ]);

    Route::get('/delete_store/{id}', [
            'uses' => 'StoreController@deleteStore',
            'as' => 'api.deleteStore'
    ]);
            /////endStore

            /////Category
    Route::post('/add_category', [
            'uses' => 'CategoryController@addCategory',
            'as' => 'api.addCategory'
    ]);

    Route::get('/delete_category/{id}', [
            'uses' => 'CategoryController@deleteCategory',
            'as' => 'api.deleteCategory'
    ]);

            /////endCategory

            /////Area
    Route::post('/add_area', [
            'uses' => 'AreaController@addArea',
            'as' => 'api.addArea'
    ]);

    Route::get('/delete_area/{id}', [
            'uses' => 'AreaController@deleteArea',
            'as' => 'api.deleteArea'
    ]);

 });
//